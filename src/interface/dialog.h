#pragma once

#include "interface/interface.h"

/**
 * Displays a dialog of information for confirmations and error messages.
 */
class Dialog : public InterfacePage
{
	Q_OBJECT

public:
	Dialog(QString title, QString message, bool confirm, QObject *parent=nullptr);
	~Dialog();

	void draw(bool fullupdate);
	void handleButton(int button);
	QString title() const;

private:
	DPtr<Dialog> d;
};
