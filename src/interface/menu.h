#pragma once

#include <QObject>

#include "dptr.h"
#include "mouse.h"
#include "interface/interface.h"

class AbstractMenuItemModel;

/**
 * Defines a menu within the interface. Provides a list of options to pick from
 * handling large collections.
 */
class Menu : public InterfacePage
{
	Q_OBJECT

public:
	class Item
	{
	public:
		Item(const QString &name);

		const QString &name() const;

	private:
		DPtr<Item> d;
	};

	Menu(const QString &menuTitle, QObject *parent=nullptr);
	~Menu();

	void draw(bool fullupdate);
	void handleButton(int button);
	AbstractMenuItemModel *model() const;
	void mouseButtonPressed(Mouse::Button button);
	void mouseMoved(int x, int y);
	void tick();
	QString title() const;
	unsigned int selected() const;
	void setModel(AbstractMenuItemModel *model);

signals:
	void backedOut();
	void itemSelected(const Menu::Item *item);

protected slots:
	void updateContents();

private:
	DPtr<Menu> d;
};
