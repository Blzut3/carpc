#include "dbus/device.h"
#include "interface/mediamodel.h"
#include "dbus.h"

struct RegisteredDevice
{
	MediaDevice *device;
	Menu::Item *item;
};

DClass<MediaModel>
{
	QList<RegisteredDevice> devices;
};

DPointeredNoCopy(MediaModel)

MediaModel::MediaModel(QObject *parent) : AbstractMenuItemModel(parent)
{
	QList<MediaDevice *> devices = DBus::instance().activeDevices();
	connect(&DBus::instance(), SIGNAL(newDevice(MediaDevice *)), SLOT(handleNewDevice(MediaDevice *)));
	for(auto dev : devices)
		handleNewDevice(dev);
}

MediaModel::~MediaModel()
{
	for(unsigned int i = 0;i < d->devices.size();++i)
		delete d->devices[i].item;
}

unsigned int MediaModel::count() const
{
	return d->devices.size();
}

void MediaModel::handleNewDevice(MediaDevice *device)
{
	device->reference(this, SLOT(handleRemovedDevice(MediaDevice *)));

	RegisteredDevice dev;
	dev.device = device;
	dev.item = new Menu::Item(device->deviceLabel());
	d->devices.append(dev);

	emit modelUpdated();
}

void MediaModel::handleRemovedDevice(MediaDevice *device)
{
	device->release(this);

	for(unsigned int i = 0;i < d->devices.size();++i)
	{
		if(d->devices[i].device == device)
			d->devices.removeAt(i);
	}
}

Menu::Item *MediaModel::item(unsigned int row) const
{
	return d->devices[row].item;
}
