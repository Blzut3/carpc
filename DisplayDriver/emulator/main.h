#include <QMainWindow>
#include <QWidget>

class Emulator : public QWidget
{
	public:
		Emulator(QWidget *parent=NULL);
		~Emulator();

		void blit(const uchar *buf, unsigned int x, unsigned int y, unsigned int w, unsigned int h);

	protected:
		void paintEvent(class QPaintEvent *event);

	private:
		class EmulatorPrivate;
		EmulatorPrivate *d;
};

class EmulatorWindow : public QMainWindow
{
	Q_OBJECT

	public:
		EmulatorWindow();
		~EmulatorWindow();

		void blit(const uchar *buf, unsigned int x, unsigned int y, unsigned int w, unsigned int h);
		int gpiCode();

	protected slots:
		void buttonReleased();
		void downPressed();
		void homePressed();
		void okPressed();
		void returnPressed();
		void upPressed();

	private:
		class EmulatorWindowPrivate;
		EmulatorWindowPrivate *d;
};
