#include "cdripper.h"
#include "dbus/device.h"
#include "udiskdeviceinterface.h"

#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/cdrom.h>
#include <QCryptographicHash>
#include <QDBusConnection>
#include <QDBusObjectPath>
#include <QDir>
#include <QHash>
#include <QMutex>

using namespace org::freedesktop::UDisks;

DClass<MediaDevice>
{
public:
	Device *dev;
	QString devPath;
	QString deviceLabel;
	QHash<const QObject *, QString> references;

	bool isPresent;
	bool mustUnmount;
	QDir path;

	bool isEjectable;
	bool isOptical;
	bool audioDisc;
	QString discTimeCodes;
	MediaDevice::AudioDiscInfo discInfo;

	QMutex refLock;
};

DPointeredNoCopy(MediaDevice)

// In the case of an error, throws a boolean stating if the drive was an
// optical drive.
MediaDevice::MediaDevice(const QDBusObjectPath &path)
{
	d->dev = new Device("org.freedesktop.UDisks", path.path(), QDBusConnection::systemBus(), this);
	d->devPath = path.path();
	qDebug() << "New device" << path.path();

	d->isPresent = true;

	// Register CD Drive
	d->isEjectable = d->dev->driveIsMediaEjectable();
	d->isOptical = false;
	if(d->isEjectable)
	{
		QStringList mediaTypes = d->dev->driveMediaCompatibility();
		foreach(const QString &type, mediaTypes)
		{
			if(type.contains("optical"))
			{
				d->isOptical = true;
				break;
			}
		}
	}

	d->mustUnmount = false;
	d->audioDisc = d->dev->deviceIsOpticalDisc();
	if(d->audioDisc)
	{
		d->deviceLabel = "CD Audio";
		d->discInfo.numTracks = d->dev->opticalDiscNumTracks();
		qDebug() << "Optical disc inserted with " << d->discInfo.numTracks << " tracks";
		if(scanAudioCD())
			new CDRipper(this);
		else if(mountFileSystem())
			scanFileSystem();
	}
	else
	{
		d->deviceLabel = d->dev->idLabel();
		if(d->deviceLabel.isEmpty())
			d->deviceLabel = d->dev->idUuid();

		// Ignore devices that aren't a scanable partition as well as the first
		// hard disk since that's not removable.
		if(!d->dev->deviceIsPartition()
			|| d->dev->deviceIsSystemInternal()
			|| d->dev->partitionType() == "0x05" // Extended partition
			|| d->dev->idType() == "swap")
			throw d->isOptical;

		// Attempt to mount our file system and reject if it fails.
		if(!mountFileSystem())
			throw d->isOptical;

		scanFileSystem();
	}
}

MediaDevice::~MediaDevice()
{
	qDebug() << "Device destructed:" << d->devPath;

	if(d->mustUnmount)
	{
		QDBusPendingReply<> reply = d->dev->FilesystemUnmount(QStringList());
		reply.waitForFinished();
	}
}

QString MediaDevice::deviceLabel() const
{
	return d->deviceLabel;
}

const MediaDevice::AudioDiscInfo &MediaDevice::discInfo() const
{
	return d->discInfo;
}

QString MediaDevice::discTimeCodes() const
{
	return d->discTimeCodes;
}

bool MediaDevice::isEjectable() const
{
	return d->isEjectable;
}

bool MediaDevice::isOptical() const
{
	return d->isOptical;
}

bool MediaDevice::isPresent() const
{
	return d->isPresent;
}

bool MediaDevice::mountFileSystem()
{
	if(!d->dev->deviceIsMounted())
	{
		// Try to mount the file system as read only (since we don't want to
		// modify anything and it allows the device to be ripped out).
		QDBusPendingReply<QString> reply = d->dev->FilesystemMount(d->dev->idType(), QStringList("ro"));
		reply.waitForFinished();
		d->mustUnmount = true;
	}

	QStringList mountPaths = d->dev->deviceMountPaths();
	if(mountPaths.size() == 0)
		return false;
	d->path = mountPaths[0];
	return true;
}

QDir MediaDevice::mountPath() const
{
	return d->path;
}

void MediaDevice::reference(const QObject *referencer, const char* callback)
{
	QMutexLocker lock(&d->refLock);
	if(d->references.contains(referencer))
		return;

	d->references.insert(referencer, callback);
	referencer->connect(this, SIGNAL(removed(MediaDevice *)), callback);
}

void MediaDevice::release(const QObject *referencer)
{
	d->refLock.lock();
	disconnect(SIGNAL(removed(MediaDevice *)), referencer, d->references[referencer].toAscii());
	d->references.remove(referencer);

	if(d->references.size() == 0)
	{
		d->refLock.unlock();
		delete this;
	}
	else
		d->refLock.unlock();
}

void MediaDevice::remove()
{
	d->isPresent = false;
	qDebug() << "Device removed:" << d->devPath;

	d->refLock.lock();
	if(d->references.size() == 0)
	{
		d->refLock.unlock();
		delete this;
	}
	else
	{
		d->refLock.unlock();
		emit removed(this);
	}
}

// Returns true for Audio, false for Data.
bool MediaDevice::scanAudioCD()
{
	static const int CDID_SIZE = 1+1+4+4*99;
	quint8 cdidData[CDID_SIZE];
	memset(cdidData, 0, CDID_SIZE);

	int fd = open(d->dev->deviceFile().toAscii(), O_RDONLY|O_NONBLOCK);
	if(fd < 0)
	{
		qDebug() << "Error reading CD!\n";
		return false;
	}

	cdrom_tochdr tocHeader;
	cdrom_tocentry entry;
	int tmp;
	entry.cdte_format = CDROM_LBA;

	ioctl(fd, CDROMREADTOCHDR, &tocHeader);
	qDebug() << tocHeader.cdth_trk0 << tocHeader.cdth_trk1;
	cdidData[0] = tocHeader.cdth_trk0;
	cdidData[1] = tocHeader.cdth_trk1;

	int &start = d->discInfo.firstSector;
	int &end = d->discInfo.lastSector;
	int &pregap = d->discInfo.pregap;

	entry.cdte_track = CDROM_LEADOUT;
	if((tmp = ioctl(fd, CDROMREADTOCENTRY, &entry)) != 0)
	{
		qDebug() << tmp;
	}

	qDebug() << entry.cdte_addr.lba;
	end = entry.cdte_addr.lba - 1; // Minus one for cdparanoia
	*(quint32*)(cdidData+2) = entry.cdte_addr.lba + 150;

	bool needStart = true;
	for(int t = tocHeader.cdth_trk0;t <= tocHeader.cdth_trk1;++t)
	{
		entry.cdte_track = t;
		// cdte_track, cdte_adr, cdte_ctrl, cdte_format, cdte_addr.(mdf,lba), cdte_datamode

		if((tmp = ioctl(fd, CDROMREADTOCENTRY, &entry)) != 0)
		{
			qDebug() << tmp;
			break;
		}

		// cdte_ctrl includes DATA flag
		qDebug() << "Track" << entry.cdte_track << entry.cdte_adr << entry.cdte_ctrl << entry.cdte_format << entry.cdte_addr.lba;

		*(quint32*)(cdidData+2+entry.cdte_track*4) = entry.cdte_addr.lba + 150;

		if(needStart)
		{
			start = entry.cdte_addr.lba;
			needStart = false;
		}

		d->discInfo.trackSeek[entry.cdte_track-1] = static_cast<quint64>(entry.cdte_addr.lba)*1000/75;
		qDebug() << d->discInfo.trackSeek[entry.cdte_track-1];

		if(entry.cdte_ctrl & CDROM_DATA_TRACK)
		{
			qDebug() << "Track is DATA!";
			if(t == tocHeader.cdth_trk0) // DATA first? (Game discs)
			{
				if(t == tocHeader.cdth_trk1) // first and last = data disc, don't rip.
				{
					close(fd);
					return false;
				}
				needStart = true;
			}
			else // DATA last? (Mixed mode music)
			{
				end = entry.cdte_addr.lba - 1;
				break;
			}
		}
	}
	// cdparanoia can't rip the pregap
	pregap = start;

	d->discInfo.trackSeek[d->discInfo.numTracks] = static_cast<quint64>(end - pregap)*1000/75;

	for(unsigned int i = 0;i < 99;++i)
	{
		// While we're doing this, check that our seek table accounts for the pregap
		if(i < d->discInfo.numTracks)
			d->discInfo.trackSeek[i] -= pregap*1000/75;

		const quint32 tmp1 = *(quint32*)(cdidData+2+i*4);
		*(quint32*)(cdidData+2+i*4) = ((tmp1&0xFF)<<24)|((tmp1&0xFF00)<<8)|((tmp1&0xFF0000)>>8)|((tmp1&0xFF000000)>>24);
	}

	QString discid = QCryptographicHash::hash(QByteArray((const char*)cdidData, CDID_SIZE).toHex().toUpper(), QCryptographicHash::Sha1).toBase64().replace('=', '-').replace('+', '.').replace('/', '_');
	memcpy(d->discInfo.discid, discid.toAscii(), 29);

	int tcStart = start - pregap;
	int tcEnd = end - pregap;
	d->discTimeCodes = QString("[%1:%2.%3]-[%4:%5.%6]").arg(tcStart/4500).arg((tcStart%4500)/75).arg(tcStart%75).arg(tcEnd/4500).arg((tcEnd%4500)/75).arg(tcEnd%75);
	qDebug() << d->discTimeCodes;

	close(fd);
	return true;
}

#include "fsmedia.h"
// Look for playable media files.
void MediaDevice::scanFileSystem()
{
	FSScanner *scanner = new FSScanner(this);
}
