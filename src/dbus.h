#pragma once

#include "dptr.h"

#include <QObject>
#include <QtContainerFwd>

class MediaDevice;
class QDBusObjectPath;

class DBus : public QObject
{
	Q_OBJECT

public:
	DBus();
	~DBus();

	bool cdInDrive() const;
	QList<MediaDevice *> activeDevices() const;

	static DBus &instance() { return *staticInstance; }

public slots:
	void checkOpticalDrive(const QDBusObjectPath &device);
	bool deviceAdded(const QDBusObjectPath &device);
	void deviceRemoved(const QDBusObjectPath &device);

signals:
	void cdStatusChanged(bool indrive);
	void newDevice(MediaDevice *device);

private:
	DPtr<DBus> d;
	static DBus *staticInstance;
};
