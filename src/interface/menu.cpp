#include "interface/menu.h"
#include "interface/menumodel.h"
#include "lcddisplay.h"

#include <cmath>
#include <QDebug>

static const int ITEM_SIZE = 13;

DClass<Menu::Item>
{
	QString label;
};

DPointered(Menu::Item)

Menu::Item::Item(const QString &name)
{
	d->label = name;
}

const QString &Menu::Item::name() const
{
	return d->label;
}

////////////////////////////////////////////////////////////////////////////////

DClass<Menu>
{
	QString title;
	AbstractMenuItemModel *model;
	int target;
	int scroll;
	int velocity;
};

DPointered(Menu)

Menu::Menu(const QString &menuTitle, QObject *parent) : InterfacePage(parent)
{
	d->title = menuTitle;
	d->model = new BasicMenuModel();
	d->scroll = 0;
	d->velocity = 0;
	d->target = -1;
}

Menu::~Menu()
{
}

void Menu::draw(bool)
{
	static const unsigned int INDEX_WIDTH = 32;
	static const unsigned int SELECTION_Y1 = (LCDDisplay::HEIGHT/2)-7;
	static const unsigned int SELECTION_Y2 = (LCDDisplay::HEIGHT/2)+6;
	LCDDisplay &display = LCDDisplay::instance();
	AbstractMenuItemModel *mdl = model();

	// Draw UI elements
	display.fillRect(0, Interface::INFO_MARGINS+1, INDEX_WIDTH, LCDDisplay::HEIGHT-Interface::INFO_MARGINS-1, 1);
	display.fillRect(INDEX_WIDTH, Interface::INFO_MARGINS+1, LCDDisplay::WIDTH, LCDDisplay::HEIGHT-Interface::INFO_MARGINS-1, 0);
	display.fillRect(INDEX_WIDTH, SELECTION_Y1, LCDDisplay::WIDTH, SELECTION_Y2, 1);

	// Draw items
	unsigned int y = SELECTION_Y1+1;
	for(unsigned int i = 0;i < mdl->count();++i)
	{
		const QString &label = mdl->item(i)->name();
		display.drawText(label, INDEX_WIDTH+1, y-d->scroll);
		y += 13;

		if(i == selected())
			display.drawText(label.left(1), INDEX_WIDTH/2, Interface::INFO_MARGINS+8, LCDDisplay::BIGFONT, LCDDisplay::ALIGN_CENTER);
	}
}

unsigned int Menu::selected() const
{
	return (d->scroll + ITEM_SIZE/2) / ITEM_SIZE;
}

void Menu::handleButton(int button)
{
	switch(button)
	{
	default: break;
	case LCDDisplay::BUTTON_Up:
		d->scroll = qMax<int>(0, selected()-1)*ITEM_SIZE;
		break;
	case LCDDisplay::BUTTON_Down:
		d->scroll = qMin<int>(d->model->count()-1, selected()+1)*ITEM_SIZE;
		break;
	case LCDDisplay::BUTTON_Ok:
		emit itemSelected(d->model->item(selected()));
		break;
	}
}

AbstractMenuItemModel *Menu::model() const
{
	return d->model;
}

QString Menu::title() const
{
	return d->title;
}

void Menu::mouseButtonPressed(Mouse::Button button)
{
}

void Menu::mouseMoved(int x, int y)
{
	static const double SENSITIVITY = 0.8;

	if(qAbs(x) < 16)
	{
		d->velocity = static_cast<int>(y*SENSITIVITY);
		if(d->velocity != 0)
			d->target = qMax<int>(0, selected() + (d->velocity > 0 ? 1 : -1));
	}
	else
	{
		if(x > 0)
			emit itemSelected(d->model->item(selected()));
	}
}

void Menu::setModel(AbstractMenuItemModel *model)
{
	delete d->model;

	if(!model)
	{
		d->model = new BasicMenuModel();
		return;
	}

	model->setParent(this);
	d->model = model;

	updateContents();
	connect(model, SIGNAL(modelUpdated()), SLOT(updateContents()));
}

static inline int DetermineAutoScrollDirection(unsigned int selected, int target, int scroll)
{
	if(target >= 0 && selected != target)
	{
		return selected > target ? -1 : 1;
	}
	return (scroll % ITEM_SIZE <= 6) ? -1 : 1;
}
void Menu::tick()
{
	if(model()->count() == 0)
		return;

	bool negVelocity = d->velocity < 0;
	d->velocity = qAbs(d->velocity);
	d->scroll += (negVelocity ? -1 : 1)*qMin(d->velocity, 10);

	d->velocity = qMax(0, d->velocity - qMax(1, static_cast<int>(log(d->velocity))));

	if(d->scroll < 0)
		d->scroll = 0;
	else if(d->scroll > (model()->count()-1)*ITEM_SIZE)
		d->scroll = (model()->count()-1)*ITEM_SIZE;

	if(d->velocity < 1)
	{
		for(unsigned int i = 0;d->scroll % ITEM_SIZE != 0 && i < 3;++i)
			d->scroll += DetermineAutoScrollDirection(selected(), d->target, d->scroll);

		if(selected() == d->target)
			d->target = -1;
	}

	if(negVelocity)
		d->velocity *= -1;
}

void Menu::updateContents()
{
	if(selected() >= d->model->count())
	{
		if(d->model->count() == 0)
			d->scroll = 0;
		else
			d->scroll = d->model->count()*ITEM_SIZE;
	}
}
