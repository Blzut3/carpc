#pragma once

#include "dptr.h"

#include <QObject>

class Mouse : public QObject
{
	Q_OBJECT

public:
	enum Button
	{
		BUTTON_LEFT,
		BUTTON_RIGHT,
		BUTTON_MIDDLE,
		BUTTON_BACK,
		BUTTON_FORWARD,
		BUTTON_SCROLLUP,
		BUTTON_SCROLLDOWN
	};
	enum Axis
	{
		AXIS_X,
		AXIS_Y
	};

	struct Data
	{
		quint8 buttons;
		qint8 axis[2];
		qint8 scrollWheel;
	};

	Mouse(const QString &device);
	~Mouse();

	qint8 axisRate(Axis axis);
	void clearAxes();
	bool hasMouseMoved() const;

signals:
	void buttonPressed(Mouse::Button button);

private:
	friend class MouseDeviceReader;
	void updateInput(const Mouse::Data &input);

	DPtr<Mouse> d;
};
