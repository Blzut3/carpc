#include <cassert>
#include <QApplication>
#include <QTimer>
#include <cddb/cddb.h>

#include "database.h"
#include "dbus.h"
#include "uicontroller.h"
#include "lcddisplay.h"
#include "mediaplayer.h"
#include "picoLCD.h"
#include "pulseaudio.h"

int main(int argc, char* argv[])
{
	bool freeApp = false;
	drv_pLG_start();
	if(!qApp)
	{
		freeApp = true;
		QApplication *app = new QApplication(argc, argv, false);
	}
	assert(qApp);

	libcddb_init();

	{
		LCDDisplay lcd;
		MediaPlayer mp;
		PulseAudio pa;
		QObject::connect(&pa, SIGNAL(volumeChanged(int)), &mp, SLOT(handleVolumeChanged(int)));

		Database db;
		DBus dbus;
		UIController::startController();

		qApp->exec();
	}

	libcddb_shutdown();

	drv_pLG_close();
	if(freeApp)
		delete qApp;
	return 0;
}
