#pragma once

#include "interface/menumodel.h"

class MediaDevice;

/**
 * Menu model which list the available media.
 */
class MediaModel : public AbstractMenuItemModel
{
	Q_OBJECT
	DPtr<MediaModel> d;

public:
	MediaModel(QObject *parent=nullptr);
	~MediaModel();

	unsigned int count() const;
	Menu::Item *item(unsigned int row) const;

protected slots:
	void handleNewDevice(MediaDevice *dev);
	void handleRemovedDevice(MediaDevice *dev);
};
 
