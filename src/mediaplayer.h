#pragma once

#include "dptr.h"

#include <QObject>

class Media;
class QFileInfo;
class QUrl;

class TimeCode
{
public:
	TimeCode() : time(0) {}
	TimeCode(unsigned int hour, unsigned int min, unsigned int sec)
	{
		time = (hour<<12)|((min&0x3F)<<6)|(sec&0x3F);
	}

	inline unsigned int hours() const { return time>>12; }
	inline unsigned int minutes() const { return (time>>6)&0x3F; }
	inline unsigned int seconds() const { return time&0x3F; }

	inline QString toString() const
	{
		return QString("%1:%2:%3")
			.arg(hours(), 1, 10, QChar('0'))
			.arg(minutes(), 2, 10, QChar('0'))
			.arg(seconds(), 2, 10, QChar('0'));
	}

	inline bool operator==(const TimeCode other) const { return time == other.time; }
	inline bool operator!=(const TimeCode other) const { return time != other.time; }
	inline bool operator>(const TimeCode other) const { return time > other.time; }
	inline bool operator>=(const TimeCode other) const { return time > other.time; }
	inline bool operator<(const TimeCode other) const { return time < other.time; }
	inline bool operator<=(const TimeCode other) const { return time <= other.time; }
private:
	unsigned int time;
};

class MediaMetaInfo
{
public:
	QString fileName;
	QString track;
	QString album;
	QString artist;
	QString tracknum;
	bool valid;
};

class MediaPlayer : public QObject
{
	Q_OBJECT

public:
	MediaPlayer();
	~MediaPlayer();

	MediaMetaInfo checkFileForMedia(const QFileInfo &fi) const;
	Media *mediaObject() const;
	void togglePause();
	TimeCode trackTime() const;
	void nextTrack();
	void prevTrack();

	static inline MediaPlayer &instance() { return *staticPlayer; }

public slots:
	void stop();

signals:
	void mediaChanged(Media *mediaObject);
	void trackFinished();
	void requestPreviousTrack();

protected:
	friend class Media;
	friend class CDRipper;

	void playMedia(const QUrl &url, const QString &options, quint64 start, quint64 end);
	void setMediaObject(Media *mediaObject);

protected slots:
	void handleVolumeChanged(int volume);

private:
	friend class VLCThread;

	void signalNextTrack();

	DPtr<MediaPlayer> d;

	static MediaPlayer *staticPlayer;
};
