#include "interface/interface.h"
#include "interface/menu.h"
#include "lcddisplay.h"
#include "dbus.h"
#include "media.h"
#include "mediaplayer.h"
#include "pulseaudio.h"

#include <QDebug>
#include <QIcon>
#include <QList>
#include <QVariant>

DClass<Interface>
{
	QList<InterfacePage *> pages;

	double slidePos;
	double lastSlidePos;
	bool cdStatus;
	bool updateIndicators;
	bool updateStatus;
};

DPointered(Interface)

Interface::Interface(QObject *parent) : QObject(parent)
{
	LCDDisplay::instance().fillRect(0, 0, LCDDisplay::WIDTH, LCDDisplay::HEIGHT, 0);

	d->slidePos = d->lastSlidePos = 0;
	d->cdStatus = DBus::instance().cdInDrive();
	d->updateIndicators = d->updateStatus = true;

	connect(&DBus::instance(), SIGNAL(cdStatusChanged(bool)), SLOT(setCDStatus(bool)));
	connect(&PulseAudio::instance(), SIGNAL(volumeChanged(int)), SLOT(setVolume(int)));
	connect(&MediaPlayer::instance(), SIGNAL(mediaChanged(Media*)), SLOT(handleMediaChanged()));
	connect(&MediaPlayer::instance(), SIGNAL(trackFinished()), SLOT(handleMediaChanged()));
}

Interface::~Interface()
{
}

bool Interface::atRoot() const
{
	return d->pages.size() <= 1;
}

void Interface::draw(bool noInterface)
{
	LCDDisplay &display = LCDDisplay::instance();
	display.setOrigin(0, 0);

	if(!noInterface)
	{
		if(d->updateIndicators)
		{
			display.fillRect(0, INFO_MARGINS, LCDDisplay::WIDTH, INFO_MARGINS, 1);
			display.fillRect(0, 0, LCDDisplay::WIDTH, INFO_MARGINS-1, 0);

			static const QRect volumeBounds(LCDDisplay::instance().measureText("20"));

			if(d->cdStatus)
			{
				QImage cdicon(":/cdin.png");
				display.drawImage(cdicon, LCDDisplay::WIDTH-volumeBounds.width()-24, 0);
			}

			QImage volicon(":/volume.png");
			display.drawImage(volicon, LCDDisplay::WIDTH-volumeBounds.width()-12, 0);
			display.drawText(QString("%1").arg(PulseAudio::instance().volume()/5), 256, 0, LCDDisplay::SMALLFONT, LCDDisplay::ALIGN_RIGHT);
		}

		MediaPlayer &mp = MediaPlayer::instance();

		if(d->updateStatus)
		{
			display.fillRect(0, LCDDisplay::HEIGHT-INFO_MARGINS, LCDDisplay::WIDTH, LCDDisplay::HEIGHT-INFO_MARGINS, 1);
			display.fillRect(0, LCDDisplay::HEIGHT-INFO_MARGINS+1, LCDDisplay::WIDTH, LCDDisplay::HEIGHT, 0);

			if(Media *media = mp.mediaObject())
			{
				qDebug() << __PRETTY_FUNCTION__;
				display.drawText(media->getMeta(Media::TRACK).toString(), 0, LCDDisplay::HEIGHT-INFO_MARGINS+1);
			}
		}

		// Update time stamp
		static TimeCode tc;
		TimeCode newtc = mp.mediaObject() ? mp.trackTime() : TimeCode();
		if(newtc != tc || d->updateStatus)
		{
			QRect bounding(display.measureText(tc.toString()));
			display.fillRect(LCDDisplay::WIDTH-bounding.width(), LCDDisplay::HEIGHT-INFO_MARGINS+1, LCDDisplay::WIDTH, LCDDisplay::HEIGHT, 0);
			display.drawText(newtc.toString(), 256, LCDDisplay::HEIGHT-INFO_MARGINS+1, LCDDisplay::SMALLFONT, LCDDisplay::ALIGN_RIGHT);

			tc = newtc;
		}
	}

	if(!d->pages.isEmpty())
	{
		InterfacePage &currentPage = *d->pages.last();

		if(d->updateIndicators)
			display.drawText(currentPage.title(), 0, 0);

		// Ensure our pages don't write over the info areas
		display.pushClip(QRect(0, INFO_MARGINS+1, LCDDisplay::WIDTH, LCDDisplay::HEIGHT-2*INFO_MARGINS-1));

		if(qAbs(d->slidePos) >= 1.0/LCDDisplay::WIDTH)
		{
			if(d->pages.size() > 1)
			{
				InterfacePage &lastPage = *d->pages[d->pages.size()-2];
				lastPage.draw(false);
			}
			else
				display.fillRect(0, INFO_MARGINS+1, LCDDisplay::WIDTH, LCDDisplay::HEIGHT-INFO_MARGINS-1, 0);
			display.setOrigin(d->slidePos*LCDDisplay::WIDTH, 0);
			currentPage.draw(true);
		}
		else
			currentPage.draw(d->lastSlidePos != d->slidePos);

		d->lastSlidePos = d->slidePos;

		display.popClip();
	}

	d->updateIndicators = d->updateStatus = false;
}

void Interface::handleMediaChanged()
{
	d->updateStatus = true;
}

InterfacePage *Interface::page() const
{
	return d->pages.last();
}

void Interface::pushPage(InterfacePage *page)
{
	d->pages.append(page);
	d->updateIndicators = true;
}

InterfacePage *Interface::popPage()
{
	InterfacePage *ret = d->pages.takeLast();
	d->updateIndicators = true;
	return ret;
}

void Interface::setCDStatus(bool indrive)
{
	d->cdStatus = indrive;
	d->updateIndicators = true;
}

void Interface::setSlide(double pos)
{
	d->slidePos = pos;
}

void Interface::setVolume(int)
{
	d->updateIndicators = true;
}

double Interface::slide() const
{
	return d->slidePos;
}
