#pragma once

#include "dptr.h"

#include <QObject>

class QImage;
class QRect;

/**
 * Display buffer handler. Manages the content of the LCD and tells the driver
 * where pixels have changed.
 *
 * There should only be one instance of this class which can be retrieved by
 * LCDDisplay::instance().
 */
class LCDDisplay : public QObject
{
	Q_OBJECT

public:
	static const unsigned int WIDTH = 256;
	static const unsigned int HEIGHT = 64;

	enum TextAlignment
	{
		ALIGN_LEFT,
		ALIGN_RIGHT,
		ALIGN_CENTER
	};

	enum FontType
	{
		SMALLFONT,
		BIGFONT
	};

	enum Button
	{
		BUTTON_Return = 1,
		BUTTON_Home,
		BUTTON_Up = 5,
		BUTTON_Ok,
		BUTTON_Down
	};

	LCDDisplay();

	void drawImage(const QImage &image, int x, int y, bool masked=false);
	void drawText(const QString &text, int x, int y, FontType fontType=SMALLFONT, TextAlignment align=ALIGN_LEFT);
	void fillRect(int x1, int y1, int x2, int y2, bool color);
	/**
	 * Returns the bounding width/height of a given string.
	 */
	QRect measureText(const QString &text, FontType fontType=SMALLFONT);
	/**
	 * Pops the clipping stack.
	 */
	void popClip();
	/**
	 * Pushes the clipping rect onto the stack. This rect will respect all
	 * clipping already set on the stack. (In otherwords everything will be
	 * clipped to the intersection of the current clip and the new one.)
	 */
	void pushClip(const QRect &rect);

	/**
	 * Changes the origin of drawing so that entire contents can be shifted.
	 * Doesn't affect the clipping region.
	 */
	void setOrigin(short x, short y);

	static inline LCDDisplay &instance() { return *staticInstance; }

public slots:
	void update();
	void wakeBacklight();

signals:
	void buttonPressed(int button);
	void buttonReleased(int button);
	void vsync();

protected:
	void makeDirty(unsigned short x1, unsigned short y1, unsigned short x2, unsigned short y2);

protected slots:
	void sleepBacklight();
	void updateButtons();

private:
	static LCDDisplay *staticInstance;

	DPtr<LCDDisplay> d;
};
