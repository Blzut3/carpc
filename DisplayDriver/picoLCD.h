#ifndef __PICOLCD_H__
#define __PICOLCD_H__

extern "C" {
	
#define SCREEN_H			64
#define SCREEN_W			256

int drv_pLG_start();
int drv_pLG_close();

int drv_pLG_contrast(int contrast);
int drv_pLG_backlight(int backlight);

void drv_pLG_blit(const unsigned char * const buffer, const unsigned int row, const unsigned int col, const unsigned int width, const unsigned int height);
int drv_pLG_gpi();

}

#endif
