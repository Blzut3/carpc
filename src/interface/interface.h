#pragma once

#include "dptr.h"
#include <QObject>

class QString;
class Media;

class InterfacePage : public QObject
{
	Q_OBJECT

public:
	using QObject::QObject;
	virtual ~InterfacePage() {}

	virtual void draw(bool fullupdate)=0;
	virtual void handleButton(int button) {}
	virtual void mouseMoved(int velx, int vely) {}
	virtual void tick() {}
	virtual QString title() const=0;
};

/**
 * Interface manager. Keeps track of the stack and serves as a starting point
 * for drawing. Also handles drawing info above and below the pages.
 */
class Interface : public QObject
{
	Q_OBJECT

public:
	static const unsigned int INFO_MARGINS = 13;

	Interface(QObject *parent=nullptr);
	~Interface();

	bool atRoot() const;
	void draw(bool noInterface=false);
	InterfacePage *page() const;
	void pushPage(InterfacePage *page);
	InterfacePage *popPage();
	void setSlide(double pos);
	double slide() const;

public slots:
	void setCDStatus(bool indrive);
	void setVolume(int volume);

protected slots:
	void handleMediaChanged();

private:
	DPtr<Interface> d;
};
