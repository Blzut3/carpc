#include "interface/dialog.h"
#include "lcddisplay.h"

DClass<Dialog>
{
	QString title;
	QString message;
	bool confirm;
};

DPointered(Dialog)

Dialog::Dialog(QString title, QString message, bool confirm, QObject *parent)
: InterfacePage(parent)
{
	d->title = title;
	d->message = message;
	d->confirm = confirm;
}

Dialog::~Dialog()
{
}

void Dialog::draw(bool)
{
	LCDDisplay &display = LCDDisplay::instance();

	display.fillRect(0, Interface::INFO_MARGINS+1, LCDDisplay::WIDTH, LCDDisplay::HEIGHT-Interface::INFO_MARGINS-1, 0);

	display.drawText(d->message, LCDDisplay::WIDTH/2, Interface::INFO_MARGINS+1, LCDDisplay::SMALLFONT, LCDDisplay::ALIGN_CENTER);

	if(d->confirm)
	{
		display.drawText("< YES", 0, LCDDisplay::HEIGHT-Interface::INFO_MARGINS-13);
		display.drawText("NO >", 256, LCDDisplay::HEIGHT-Interface::INFO_MARGINS-13, LCDDisplay::SMALLFONT, LCDDisplay::ALIGN_RIGHT);
	}
	else
	{
		display.drawText("< OK", 0, LCDDisplay::HEIGHT-Interface::INFO_MARGINS-13);
	}
}

#include "uicontroller.h"
void Dialog::handleButton(int button)
{
	if(button == LCDDisplay::BUTTON_Ok)
		UIController::instance().handlePageExit();
}

QString Dialog::title() const
{
	return d->title;
}
