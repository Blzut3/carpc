#pragma once

#include "dptr.h"

#include <QObject>

class QFile;
class QUrl;

class Media : public QObject
{
	Q_OBJECT

public:
	enum MetaInformation
	{
		ARTIST,
		ALBUM,
		TRACK,
		LOCATION
	};

	Media(const QFile &file);
	virtual ~Media();

	QVariant getMeta(MetaInformation meta) const;
	bool isPlaying() const;

	void reference();
	void dereference();

public slots:
	virtual void play();
	virtual void stop();

signals:
	/**
	 * Emitted when control should be reverted back to the playlist.
	 */
	void finished();
	void metaChanged(Media::MetaInformation meta);
	void starved();

protected:
	Media();

	void doPlay(const QUrl &url, const QString &options, float start, float end);
	void setMeta(MetaInformation meta, QVariant data);

protected slots:
	friend class MediaPlayer;

	/**
	 * Called when the media player has completed the play call.
	 */
	virtual void finish();
	/**
	 * Called when the media player finishes before reaching the end point.
	 */
	virtual void starve();

	// Probably don't actually want this, but since our media object are
	// currently multitrack, this will signal the desire to go back.
	virtual void previousTrack() {}

private:
	DPtr<Media> d;
};
