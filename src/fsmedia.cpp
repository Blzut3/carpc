#include "fsmedia.h"
#include "mediaplayer.h"
#include "dbus/device.h"

#include <QDebug>
#include <QDir>
#include <QMutex>
#include <QQueue>
#include <QTemporaryFile>
#include <QUrl>
#include <QtConcurrentRun>

DClass<FSScanner>
{
	MediaDevice *dev;

	bool scanning;
	QFuture<void> scanner;
};

DPointered(FSScanner)

FSScanner::FSScanner(MediaDevice *dev)
{
	d->dev = dev;
	dev->reference(this, SLOT(mediaRemoved()));

	d->scanning = true;

	d->scanner = QtConcurrent::run([this](){this->scan();});
}

FSScanner::~FSScanner()
{
	if(d->dev)
		d->dev->release(this);
}

void FSScanner::scan()
{
	static const QStringList SCAN_EXTENSIONS {
		"*.ogg", "*.opus", "*.mp2", "*.mp3", "*.flac", "*.wma", "*.m4a",
		"*.mp4", "*.wav", "*.mka", "*.it", "*.s3m", "*.xm", "*.mid", "*.mod"
	};

	QQueue<QDir> dirs;
	QQueue<QFileInfo> candidateMedia;
	dirs.enqueue(d->dev->mountPath());

	do
	{
		QDir dir = dirs.dequeue();

		QFileInfoList entries = dir.entryInfoList(SCAN_EXTENSIONS, QDir::NoDotAndDotDot|QDir::AllDirs|QDir::Dirs|QDir::Files, QDir::Name|QDir::DirsFirst|QDir::IgnoreCase);
		foreach(const QFileInfo &f, entries)
		{
			if(f.isDir())
				dirs.enqueue(f.absoluteFilePath());
			else
				candidateMedia.enqueue(f);
		}
	}
	// Always do a check if the device is present since it can disappear at any time.
	while(!dirs.isEmpty() && d->dev->isPresent());

	bool played = false;
	FSMedia *media = new FSMedia(d->dev);
	while(!candidateMedia.isEmpty() && d->dev->isPresent())
	{
		QFileInfo fi = candidateMedia.dequeue();
		MediaMetaInfo inf = MediaPlayer::instance().checkFileForMedia(fi);
		if(inf.valid)
		{
			qDebug() << "Found playable:" << fi.absoluteFilePath();
			media->addMedia(inf);
			if(!played)
			{
				media->play();
				played = true;
			}
		}
	}

	d->scanning = false;
}

void FSScanner::mediaRemoved()
{
	d->scanner.waitForFinished();
	d->dev->release(this);
	d->dev = NULL;

	delete this;
}

//------------------------------------------------------------------------------

enum { MAX_MEDIA_CACHE = 3 };

DClass<FSMedia>
{
	MediaDevice *dev;
	QList<MediaMetaInfo> media;
	QTemporaryFile cacheFiles[MAX_MEDIA_CACHE];
	int cached;
	int pos;

	QMutex cacheMutex;
};

DPointeredNoCopy(FSMedia)

FSMedia::FSMedia(MediaDevice *dev)
{
	d->dev = dev;
	dev->reference(this, SLOT(mediaRemoved()));

	d->pos = 0;
	d->cached = 0;

	for(unsigned i = 0;i < MAX_MEDIA_CACHE;++i)
	{
		d->cacheFiles[i].open();
		d->cacheFiles[i].close();
	}

	setMeta(Media::LOCATION, d->dev->deviceLabel());
}

FSMedia::~FSMedia()
{
	if(d->dev)
		d->dev->release(this);
}

void FSMedia::addMedia(const MediaMetaInfo &mi)
{
	d->media.append(mi);
	checkCache();
}

void FSMedia::checkCache()
{
	// Can't cache if the device is gone.
	if(!d->dev)
		return;

	d->cacheMutex.lock();
	while(d->cached < d->pos+MAX_MEDIA_CACHE && d->cached < d->media.size())
	{
		QFile file(d->media[d->cached].fileName);
		// Remove the temp file without reseting the object since we want to
		// copy data into it.
		QFile(d->cacheFiles[d->cached%MAX_MEDIA_CACHE].fileName()).remove();
		if(file.copy(d->cacheFiles[d->cached%MAX_MEDIA_CACHE].fileName()))
			qDebug() << "Cached" << d->media[d->cached].fileName << "to" << d->cacheFiles[d->cached%MAX_MEDIA_CACHE].fileName();
		else
			qDebug() << "Failed caching" << d->media[d->cached].fileName << "to" << d->cacheFiles[d->cached%MAX_MEDIA_CACHE].fileName();
		++d->cached;
	}
	d->cacheMutex.unlock();
}

void FSMedia::finish()
{
	qDebug() << __PRETTY_FUNCTION__;
	if(++d->pos < d->media.size())
	{
		play();
		checkCache();
	}
	else
		emit finished();
}

void FSMedia::mediaRemoved()
{
	d->dev->release(this);
	d->dev = NULL;

	// Clear out data we'll never be able to access now.
	d->media.erase(d->media.begin()+d->cached, d->media.end());
}

void FSMedia::play()
{
	if(d->pos < d->media.size())
	{
		const auto &media = d->media[d->pos];
		const auto &cache = d->cacheFiles[d->pos%MAX_MEDIA_CACHE];

		qDebug() << "Playing media:" << media.fileName << cache.fileName();
		setMeta(Media::TRACK, media.track);
		setMeta(Media::ARTIST, media.artist);
		setMeta(Media::ALBUM, media.album);

		d->cacheMutex.lock();
		// TODO: Figure out why I require start/end here besides because I only supported CD
		doPlay(QUrl(QString("file://%1").arg(cache.fileName())), "", 0, -1);
		d->cacheMutex.unlock();
	}
}

void FSMedia::previousTrack()
{
	if(--d->pos < 0)
		d->pos = d->media.size()-1;
	d->cached = d->pos-1;
	checkCache();
	play();
}
