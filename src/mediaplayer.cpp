#include "media.h"
#include "mediaplayer.h"

#include <unistd.h>
#include <cstdlib>
#include <vlc/vlc.h>
#include <QDebug>
#include <QFileInfo>
#include <QThread>
#include <QMutex>
#include <QUrl>

class VLCThread;

MediaPlayer *MediaPlayer::staticPlayer;

DClass<MediaPlayer>
{
public:
	QMutex vlcMutex;

	libvlc_instance_t *vlc;
	libvlc_media_player_t *mp;
	libvlc_media_t *media;
	QUrl mediaURL;
	QString mediaOptions;

	VLCThread *thread;

	qint64 startPoint;
	qint64 watchPoint; // -1 disarms watchpoint
	qint64 timeStamp;
	Media *currentMediaObject;
};

DPointeredNoCopy(MediaPlayer)

class VLCThread : public QThread
{
public:
	VLCThread(MediaPlayer * const mp) : QThread(mp), mp(mp)
	{
	}

	static void handleTimeChanged(const libvlc_event_t *event, void *data)
	{
		MediaPlayer * const mp = reinterpret_cast<VLCThread *>(data)->mp;
		if(mp->d->watchPoint != -1 && mp->d->watchPoint <= event->u.media_player_time_changed.new_time)
			mp->signalNextTrack();
		//qDebug() << event->u.media_player_time_changed.new_time;
		mp->d->timeStamp = event->u.media_player_time_changed.new_time;
	}

	static void handleEndReached(const libvlc_event_t *, void *data)
	{
		MediaPlayer * const mp = reinterpret_cast<VLCThread *>(data)->mp;
		mp->signalNextTrack();
	}

	void run()
	{
		mp->d->vlc = libvlc_new(0, NULL);
		mp->d->mp = libvlc_media_player_new(mp->d->vlc);
		qDebug() << "VLC has started!";

		auto em = libvlc_media_player_event_manager(mp->d->mp);
		libvlc_event_attach(em, libvlc_MediaPlayerTimeChanged, handleTimeChanged, this);
		libvlc_event_attach(em, libvlc_MediaPlayerEndReached, handleEndReached, this);

		mp->d->vlcMutex.unlock();
		exec();
	}

private:
	MediaPlayer * const mp;
};

MediaPlayer::MediaPlayer()
{
	if(!staticPlayer)
		staticPlayer = this;

	d->vlcMutex.lock();

	qDebug() << "Starting VLC media player:" << libvlc_get_version();

	d->vlc = NULL;
	d->media = NULL;
	d->currentMediaObject = NULL;
	d->startPoint = 0;
	d->watchPoint = -1;
	d->timeStamp = 0;

	d->thread = new VLCThread(this);
	d->thread->start();
}

MediaPlayer::~MediaPlayer()
{
	d->thread->quit();
	d->thread->wait();

	libvlc_media_player_release(d->mp);
	libvlc_release(d->vlc);
}

MediaMetaInfo MediaPlayer::checkFileForMedia(const QFileInfo &fi) const
{
	// Wait for VLC to be initialized.  I don't think we need the mutex locked
	// for this?
	while(!d->vlc);

	libvlc_media_t *media = libvlc_media_new_path(d->vlc, fi.absoluteFilePath().toUtf8().constData());

	libvlc_media_parse(media);

	MediaMetaInfo inf;
	if(libvlc_media_get_duration(media) > 0)
	{
		inf.valid = true;
		inf.fileName = fi.absoluteFilePath();
		inf.track = libvlc_media_get_meta(media, libvlc_meta_Title);
		inf.artist = libvlc_media_get_meta(media, libvlc_meta_Artist);
		inf.album = libvlc_media_get_meta(media, libvlc_meta_Album);
		inf.tracknum = libvlc_media_get_meta(media, libvlc_meta_TrackNumber);
	}
	else
		inf.valid = false;

	libvlc_media_release(media);
	return inf;
}

void MediaPlayer::handleVolumeChanged(int volume)
{
	// Mute when volume goes to 0 and unmute when going above 0.
	d->vlcMutex.lock();
	libvlc_media_player_set_pause(d->mp, volume == 0);
	d->vlcMutex.unlock();
}

Media *MediaPlayer::mediaObject() const
{
	return d->currentMediaObject;
}

void MediaPlayer::nextTrack()
{
	signalNextTrack();
}
void MediaPlayer::prevTrack()
{
	emit requestPreviousTrack();
}

void MediaPlayer::playMedia(const QUrl &url, const QString &options, quint64 start, quint64 end)
{
	d->vlcMutex.lock();

	if(!d->media || url != d->mediaURL || options != d->mediaOptions)
	{
		if(d->media)
			libvlc_media_release(d->media);
		d->media = libvlc_media_new_location(d->vlc, url.toString().toAscii());
		if(!options.isEmpty())
			libvlc_media_add_option(d->media, options.toAscii());
		libvlc_media_player_set_media(d->mp, d->media);
		libvlc_media_player_play(d->mp);

		d->mediaURL = url;
		d->mediaOptions = options;
		d->timeStamp = 0;
	}

	d->startPoint = start;
	d->watchPoint = end;
	if(abs(d->timeStamp - start) > 1000)
		libvlc_media_player_set_time(d->mp, start);

	d->vlcMutex.unlock();

	static bool HACK = true;
	if(HACK)
	{
		HACK = false;
		sleep(1);
		playMedia(url, options, start, end);
	}
}

void MediaPlayer::setMediaObject(Media *mediaObject)
{
	if(d->currentMediaObject == mediaObject)
		return;

	if(d->currentMediaObject)
	{
		disconnect(d->currentMediaObject);
		d->currentMediaObject->stop();
		d->currentMediaObject->dereference();
		d->currentMediaObject = NULL;
		stop();
	}

	d->currentMediaObject = mediaObject;
	if(d->currentMediaObject)
	{
		// We might create media object on processing threads, so when we go to
		// play them, put them on a more sensible thread so that singals are
		// sure to go through.
		d->currentMediaObject->moveToThread(thread());
		d->currentMediaObject->reference();
		d->currentMediaObject->connect(this, SIGNAL(trackFinished()), SLOT(finish()));
		d->currentMediaObject->connect(this, SIGNAL(requestPreviousTrack()), SLOT(previousTrack()));
	}
	emit mediaChanged(mediaObject);
}

void MediaPlayer::signalNextTrack()
{
	d->watchPoint = -1;
	emit trackFinished();
}

void MediaPlayer::stop()
{
	d->vlcMutex.lock();
	if(d->currentMediaObject)
	{
		d->currentMediaObject->stop();
		setMediaObject(NULL);
	}
	libvlc_media_player_stop(d->mp);
	d->timeStamp = 0;

	if(d->media)
	{
		libvlc_media_release(d->media);
		d->media = NULL;
	}
	d->vlcMutex.unlock();
}

void MediaPlayer::togglePause()
{
	d->vlcMutex.lock();
	libvlc_media_player_pause(d->mp);
	d->vlcMutex.unlock();
}


TimeCode MediaPlayer::trackTime() const
{
	qint64 codeStamp = d->timeStamp - d->startPoint;
	if(codeStamp > 0)
		return TimeCode(codeStamp/3600000, (codeStamp/60000)%60, (codeStamp/1000)%60);
	return TimeCode(0, 0, 0);
}
