#include "lcddisplay.h"
#include "picoLCD.h"

#include <cassert>
#include <QDebug>
#include <QImage>
#include <QFont>
#include <QFontMetrics>
#include <QPainter>
#include <QRect>
#include <QStack>
#include <QTimer>

#define TARGET_FRAMERATE 8

DClass<LCDDisplay>
{
public:
	unsigned short lastButton;

	QFont font[2];
	QTimer refresher;
	QTimer buttonRefresher;
	QTimer backlightTimeout;
	QStack<QRect> clip;
	short drawofsx, drawofsy;

	unsigned short dirtyx1, dirtyx2, dirtyy1, dirtyy2;
	bool bl;

	void adjustCoords(int &x, int &y)
	{
		x += drawofsx;
		y += drawofsy;
	}
};

DPointeredNoCopy(LCDDisplay)

LCDDisplay *LCDDisplay::staticInstance;
static unsigned char buffer[LCDDisplay::WIDTH*LCDDisplay::HEIGHT];
static unsigned char oldbuffer[LCDDisplay::WIDTH*LCDDisplay::HEIGHT];

LCDDisplay::LCDDisplay()
{
	if(!staticInstance)
		staticInstance = this;

	d->lastButton = 0;
	memset(buffer, 0, sizeof(buffer));
	memset(oldbuffer, 0, sizeof(oldbuffer));

	assert(LCDDisplay::WIDTH == SCREEN_W && LCDDisplay::HEIGHT == SCREEN_H);

	d->font[0] = QFont("Fixedsys Excelsior 3.01", 16, QFont::Normal);
	d->font[0].setCapitalization(QFont::SmallCaps);
	d->font[1] = QFont("Times", 24, QFont::Bold);
	d->font[1].setCapitalization(QFont::SmallCaps);

	connect(&d->refresher, SIGNAL(timeout()), SLOT(update()));
	d->refresher.setInterval(1000/TARGET_FRAMERATE);
	d->refresher.start();

	d->clip.push(QRect(0, 0, LCDDisplay::WIDTH, LCDDisplay::HEIGHT));
	d->drawofsx = d->drawofsy = 0;
	d->dirtyx1 = d->dirtyx2 = d->dirtyy1 = d->dirtyy2 = 0;

	QImage logo(":/logo.png");
	drawImage(logo, 0, 0);

	connect(&d->buttonRefresher, SIGNAL(timeout()), SLOT(updateButtons()));
	d->buttonRefresher.setInterval(1000/18);
	d->buttonRefresher.start();

	// Have backlight turn off after 5 seconds of inactivity.
	connect(&d->backlightTimeout, SIGNAL(timeout()), SLOT(sleepBacklight()));
	d->bl = true;
	d->backlightTimeout.setSingleShot(true);
	d->backlightTimeout.setInterval(5000);
	d->backlightTimeout.start();
}

void LCDDisplay::drawImage(const QImage &source, int x, int y, bool masked)
{
	d->adjustCoords(x, y);

	const QRect &clip = d->clip.top();
	QImage image(source);

	// Ensure 1bpp
	if(image.format() != QImage::Format_Mono)
		image.convertToFormat(QImage::Format_Mono);

	const unsigned int startX = qMax<int>(clip.left(), x);
	const unsigned int startY = qMax<int>(clip.top(), y);
	const unsigned int w = (x+image.width() <= clip.right() ? image.width() : qMax<int>(0, clip.right()+1-x));
	const unsigned int h = (y+image.height() <= clip.bottom() ? image.height() : qMax<int>(0, clip.bottom()+1-y));
	if(w == 0 || h == 0 || int(x+w) <= clip.left() || int(y+h) <= clip.top())
		return;
	unsigned char* rowStart = &buffer[(startY*WIDTH)+startX];
	unsigned char* out = rowStart;

	makeDirty(startX, startY, x+w, y+h);

	for(unsigned int py = startY-y;py < h;++py)
	{
		const uchar* line = image.scanLine(py);
		for(unsigned int px = 0;px < w;px += 8)
		{
			unsigned char value = *line++;
			unsigned short i = 8;
			unsigned short iStop = 0;
			if(w-px < 8)
			{
				i = w-px;
				value >>= 8-i;
			}
			if(px < startX-x)
				iStop = (startX-x)%8;

			if(masked)
			{
				while(i-- > iStop)
				{
					if((value>>i)&1)
						++out;
					else
						*out++ ^= 1;
				}
			}
			else
			{
				while(i-- > iStop)
					*out++ = !((value>>i)&1);
			}
		}
		out = (rowStart += WIDTH);
	}
}

void LCDDisplay::drawText(const QString &text, int x, int y, FontType fontType, TextAlignment align)
{
	if(text.isEmpty())
		return;

	const QRect boundingRect(measureText(text, fontType));

	const unsigned int width = boundingRect.width()+1;
	const unsigned int height = d->font[fontType].pointSize();

	QImage image(width, height, QImage::Format_Mono);
	image.fill(qRgb(255, 255, 255));

	QPainter painter(&image);
	painter.setFont(d->font[fontType]);
	// Fixedsys has a 4 pixel overhead line at 16pt
	painter.drawText(-boundingRect.left(), height-(fontType == SMALLFONT ? 4 : 0), text);

	switch(align)
	{
		default: break;
		case ALIGN_RIGHT:
			x -= width-1;
			break;
		case ALIGN_CENTER:
			x -= width/2;
			break;
	}

	drawImage(image, x, y, true);
}

void LCDDisplay::fillRect(int x1, int y1, int x2, int y2, bool color)
{
	d->adjustCoords(x1, y1);
	d->adjustCoords(x2, y2);

	const QRect &clip = d->clip.top();

	// Adjust so we're within the clipping rectangle
	if(x1 > clip.right())
		x1 = clip.right();
	else if(x1 < clip.left())
		x1 = clip.left();
	if(x2 > clip.right())
		x2 = clip.right();
	else if(x2 < clip.left())
		x2 = clip.left();
	if(y1 > clip.bottom())
		y1 = clip.bottom();
	else if(y1 < clip.top())
		y1 = clip.top();
	if(y2 > clip.bottom())
		y2 = clip.bottom();
	else if(y2 < clip.top())
		y2 = clip.top();

	makeDirty(x1, y1, x2+1, y2+1);

	unsigned char* rowStart = &buffer[(y1*WIDTH)+x1];
	for(unsigned int y = y2-y1+1;y-- > 0;)
	{
		memset(rowStart, color, x2-x1+1);
		rowStart += WIDTH;
	}
}

void LCDDisplay::makeDirty(unsigned short x1, unsigned short y1, unsigned short x2, unsigned short y2)
{
	assert(x1 <= LCDDisplay::WIDTH && x2 <= LCDDisplay::WIDTH && y1 <= LCDDisplay::HEIGHT && y2 <= LCDDisplay::HEIGHT);
	d->dirtyx1 = qMin(x1, d->dirtyx1);
	d->dirtyx2 = qMax(x2, d->dirtyx2);

	d->dirtyy1 = qMin(y1, d->dirtyy1);
	d->dirtyy2 = qMax(y2, d->dirtyy2);
}

QRect LCDDisplay::measureText(const QString &text, FontType fontType)
{
	QFontMetrics metrics(d->font[fontType]);
	return metrics.boundingRect(text);
}

void LCDDisplay::popClip()
{
	d->clip.pop();
}

void LCDDisplay::pushClip(const QRect &rect)
{
	const QRect &currentClip = d->clip.top();
	d->clip.push(rect.intersected(currentClip));
}

void LCDDisplay::setOrigin(short x, short y)
{
	d->drawofsx = x;
	d->drawofsy = y;
}

void LCDDisplay::sleepBacklight()
{
	if(d->bl)
		drv_pLG_backlight(false);
	d->bl = false;
}

void LCDDisplay::update()
{
	// Check to see if there has been anything drawn
	if((d->dirtyx1|d->dirtyx2|d->dirtyy1|d->dirtyy2) == 0)
	{
		emit vsync();
		return;
	}

	// Refine dirty area
	unsigned int dirtyx1 = LCDDisplay::WIDTH, dirtyx2 = 0, dirtyy1 = LCDDisplay::HEIGHT, dirtyy2 = 0;
	for(unsigned int y = d->dirtyy1;y < d->dirtyy2;++y)
	//for(unsigned int y = 0;y < 64;++y)
	{
		for(unsigned int x = d->dirtyx1;x < d->dirtyx2;++x)
		//for(unsigned int x = 0;x < 256;++x)
		{
			const unsigned int offset = y*LCDDisplay::WIDTH + x;
			if(oldbuffer[offset] == buffer[offset])
				continue;
			if(dirtyx1 > x)
				dirtyx1 = x;
			if(dirtyx2 < x)
				dirtyx2 = x;
			if(dirtyy1 > y)
				dirtyy1 = y;
			if(dirtyy2 < y)
				dirtyy2 = y;
		}
	}
	memcpy(oldbuffer, buffer, sizeof(buffer));
	d->dirtyx1 = d->dirtyx2 = d->dirtyy1 = d->dirtyy2 = 0;

	if(dirtyx1 > dirtyx2 || dirtyy1 > dirtyy2)
	{
		emit vsync();
		return;
	}

	drv_pLG_blit(buffer, dirtyy1, dirtyx1, dirtyx2-dirtyx1+1, dirtyy2-dirtyy1+1);

	emit vsync();
}

void LCDDisplay::updateButtons()
{
	unsigned short button = drv_pLG_gpi();
	if(button != d->lastButton)
	{
		if(d->lastButton)
			emit buttonReleased(d->lastButton);
		if(button)
		{
			wakeBacklight();
			emit buttonPressed(button);
		}
		d->lastButton = button;
	}
}

void LCDDisplay::wakeBacklight()
{
	if(!d->bl)
		drv_pLG_backlight(true);
	d->bl = true;
	d->backlightTimeout.start();
}
