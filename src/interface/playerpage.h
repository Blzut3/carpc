#pragma once

#include "dptr.h"
#include "media.h"
#include "interface/interface.h"

#include <QObject>

/**
 * Main page.  Displays the current playing track and what not.
 */
class PlayerPage : public InterfacePage
{
	Q_OBJECT

public:
	PlayerPage(QObject *parent=nullptr);
	~PlayerPage();

	void handleButton(int button);
	void draw(bool fullupdate);
	void mouseMoved(int x, int y);
	QString title() const;

protected slots:
	void updateMeta(Media::MetaInformation meta);
	void mediaChanged(Media *mediaObject);

private:
	DPtr<PlayerPage> d;
};
