#pragma once

#include <QObject>

#include "dptr.h"

class QDBusObjectPath;
class QDir;

class MediaDevice : public QObject
{
	Q_OBJECT

public:
	struct AudioDiscInfo
	{
	public:
		char discid[29];
		quint8 numTracks;
		quint64 trackSeek[100];
		int firstSector;
		int lastSector;
		int pregap;
	};

	MediaDevice(const QDBusObjectPath &path);
	~MediaDevice();

	QString deviceLabel() const;

	const AudioDiscInfo &discInfo() const;
	QString discTimeCodes() const;

	bool isEjectable() const;
	bool isOptical() const;
	bool isPresent() const;

	QDir mountPath() const;

	// Reference counting functions
	// When the removed signal is emitted this object wants to die.
	// So anything referencing it should release it.
	void reference(const QObject *referencer, const char* callback);
	void release(const QObject *referencer);

public slots:
	void remove();

signals:
	void removed(MediaDevice *);

private:
	bool mountFileSystem();
	bool scanAudioCD();
	void scanFileSystem();

	DPtr<MediaDevice> d;
};
