#pragma once

#include "dptr.h"

class QStringList;

class Database
{
public:
	Database();
	~Database();

	QStringList artistNames() const;
private:
	DPtr<Database> d;
};
