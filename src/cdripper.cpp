#include "cdripper.h"
#include "dbus/device.h"
#include "mediaplayer.h"

#include <cassert>
#include <QDebug>
#include <QProcess>
#include <QTextStream>
#include <QUrl>

enum RipState
{
	// First stage. Rip without paranoia so that we can play the disc.
	RIP_QUICK,

	// Second stage. Rip with paranoia for storage into database.
	RIP_PARANOIA,

	// Third stage, encoding
	RIP_OPUS,

	// Forth stage, committing
	RIP_COMMIT,

	// If QUICK or PARANOIA stage fails, go to this state where we just tell VLC to play the disc.
	RIP_FAIL
};

DClass<CDRipper>
{
public:
	MediaDevice *dev;
	CDMedia *media;

	RipState state;
	QProcess cdparanoia;
};

DPointeredNoCopy(CDRipper)

CDRipper::CDRipper(MediaDevice *dev)
{
	d->dev = dev;
	dev->reference(this, SLOT(discRemoved()));

	d->media = new CDMedia(this);
	d->media->reference();
	d->media->connect(this, SIGNAL(ripFinished()), SLOT(finishRip()));

	connect(&d->cdparanoia, SIGNAL(readyReadStandardError()), SLOT(paranoiaProgress()));
	connect(&d->cdparanoia, SIGNAL(finished(int, QProcess::ExitStatus)), SLOT(paranoiaFinished(int, QProcess::ExitStatus)));
	d->state = RIP_QUICK;

	startRip();
}

CDRipper::~CDRipper()
{
	qDebug() << "Destroying CDRipper";

	if(d->cdparanoia.state() == QProcess::Running)
		d->cdparanoia.terminate();

	d->media->dereference();
}

void CDRipper::discRemoved()
{
	if(d->cdparanoia.state() == QProcess::Running)
	{
		d->state = RIP_FAIL;
		d->cdparanoia.terminate();
	}

	d->dev->release(this);

	delete this;
}

void CDRipper::startRip()
{
	// Ensure we want to do a rip
	if(d->state != RIP_QUICK && d->state != RIP_PARANOIA)
		return;

	QStringList params;
	params << "-e";

	if(d->state == RIP_QUICK)
		params << "-Y";

	params << d->dev->discTimeCodes();
	params << QString("/tmp/%1%2.wav").arg(d->dev->discInfo().discid).arg(d->state == RIP_PARANOIA ? "paranoia" : "");

	qDebug() << "Ripping params:" << params;
	d->cdparanoia.start("cdparanoia", params, QIODevice::ReadOnly);
}

void CDRipper::paranoiaFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
	qDebug() << "Rip finished:" << exitCode << exitStatus;

	if(exitStatus == QProcess::CrashExit || exitCode != 0)
	{
		d->state = RIP_FAIL;
		return;
	}

	if(d->state == RIP_QUICK)
	{
		//d->state = RIP_PARANOIA;
		//startRip();
		emit ripFinished();
	}
	else
	{
		d->state = RIP_OPUS;
	}
}

void CDRipper::paranoiaProgress()
{
	QByteArray data = d->cdparanoia.readAllStandardError();
	if(data[0] != '#' || data[1] != '#')
		return;

	const MediaDevice::AudioDiscInfo &discInfo = d->dev->discInfo();
	QTextStream stream(data);
	QStringList lines;
	while(!stream.atEnd())
		lines << stream.readLine();

	foreach(QString line, lines)
	{
		stream.setString(&line, QIODevice::ReadOnly);
		int type, sector;
		QString name;

		stream.seek(4);
		stream >> type >> name;
		stream.skipWhiteSpace();
		stream.read(1);
		stream >> sector;

		if(type == -2)
		{
			sector /= 1176;
			//qDebug() << "Progress: " << double(sector)/discInfo.lastSector;

			// Start playing after we made some arbitrary amount of progress
			if(sector*1000/discInfo.lastSector >= 4 && !d->media->isPlaying())
				d->media->play();
		}
	}
}

//------------------------------------------------------------------------------

DClass<CDMedia>
{
public:
	CDRipper *ripper;
	MediaDevice::AudioDiscInfo discInfo;

	QString file;
	quint8 track;
};

DPointered(CDMedia)

CDMedia::CDMedia(CDRipper *ripper) : Media()
{
	memset(&d->discInfo, 0, sizeof(d->discInfo));

	d->track = 1;
	d->ripper = ripper;

	setMeta(Media::LOCATION, "CD Audio");
}

CDMedia::~CDMedia()
{
	qDebug() << "Destroying CDMedia";
}

void CDMedia::play()
{
	if(d->ripper && d->discInfo.numTracks == 0)
	{
		d->discInfo = d->ripper->d->dev->discInfo();
		d->file = QString("file:///tmp/%1.wav").arg(d->discInfo.discid);
	}

	// Ensure we know something about the disc
	assert(d->discInfo.numTracks);

	const float start = d->discInfo.trackSeek[d->track-1];
	const float end = d->discInfo.trackSeek[d->track];
	qDebug() << "Playing CD track" << d->track << start << end;
	doPlay(QUrl(d->file), "", start, end);
	setMeta(Media::TRACK, QString("Track %1").arg(d->track));
}

void CDMedia::finish()
{
	if(d->ripper && d->ripper->d->state == RIP_FAIL)
	{
		emit finished();
	}
	else
	{
		++d->track;
		if(d->track > d->discInfo.numTracks)
		{
			d->track = 1;
			emit finished();
		}
		else
			play();
	}
}

void CDMedia::finishRip()
{
	delete d->ripper;
	d->ripper = NULL;
}
