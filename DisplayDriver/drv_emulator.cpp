#include "emulator/main.h"
#include "picoLCD.h"

#include <cassert>
#include <cstdio>
#include <QApplication>

static int argc = 1;
static char arg1[11] = {'.','/','e','m','u','l','a','t','o','r',0};
static char* argv[1] = { arg1 };

static QApplication *app;
static EmulatorWindow *window;

extern "C" {
	int drv_pLG_start()
	{
		app = new QApplication(argc, argv);
		window = new EmulatorWindow();
		window->show();
		return 0;
	}

	int drv_pLG_close()
	{
		delete window;
		delete app;
		return 0;
	}

	int drv_pLG_contrast(int contrast)
	{
		return 0;
	}
	int drv_pLG_backlight(int backlight)
	{
		printf("Backlight status: %d\n", backlight);
		return 0;
	}

	void drv_pLG_blit(const unsigned char * const buffer, const unsigned int row, const unsigned int col, const unsigned int width, const unsigned int height)
	{
		if(!(row >= 0 && col >= 0 && row + height <= SCREEN_H && col + width <= SCREEN_W))
		{
			printf("Invalid blit region %d %d %d %d\n", row, col, width, height);
			assert(row >= 0 && col >= 0 && row + height <= SCREEN_H && col + width <= SCREEN_W);
		}

		static const bool inverted = 1;
		if(inverted)
		{
			static unsigned char buffer2[SCREEN_H*SCREEN_W];
			for(unsigned int i = 0;i < SCREEN_H*SCREEN_W;++i)
				buffer2[i] = buffer[i]^1;

			window->blit(buffer2, col, row, width, height);
		}
		else
			window->blit(buffer, col, row, width, height);
	}
	int drv_pLG_gpi()
	{
		return window->gpiCode();
	}
}
