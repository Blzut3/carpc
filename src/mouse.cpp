#include "mouse.h"

#include <cassert>
#include <QDebug>
#include <QFile>
#include <QThread>

class MouseDeviceReader;

DClass<Mouse>
{
	QFile device;
	MouseDeviceReader *reader;
	Mouse::Data oldData;
	bool hasMoved;
};

DPointeredNoCopy(Mouse)

class MouseDeviceReader : public QThread
{
public:
	MouseDeviceReader(Mouse *device) : QThread(device), device(device)
	{
	}

	void run()
	{
		QByteArray data;
		Mouse::Data out;
		do
		{
			data = device->d->device.read(4);
			out.buttons = (data[0]&7)|((data[3]&48)>>1);
			out.axis[0] = data[1];
			out.axis[1] = data[2];
			out.scrollWheel = data[3]&15;
			if(out.scrollWheel&0x8)
				out.scrollWheel |= 0xF0;

			device->updateInput(out);
		}
		while(true);
	}
private:
	Mouse *device;
};

Mouse::Mouse(const QString &device)
{
	qRegisterMetaType<Mouse::Button>("Mouse::Button");

	d->device.setFileName(device);
	if(!d->device.open(QIODevice::ReadWrite|QIODevice::Unbuffered))
		qDebug() << "Could not open mouse:" << device;
	else
		qDebug() << "Mouse opened:" << device;

	const unsigned char intellimouse[7] = { 0xF3, 200, 0xF3, 200, 0xF3, 80, 0xF2 };
	d->device.write((const char*) intellimouse, 7);
	QByteArray id = d->device.read(2);
	assert((quint8)id[1] == 4);

	d->reader = new MouseDeviceReader(this);
	d->reader->start();

	d->hasMoved = false;
	memset(&d->oldData, 0, sizeof(Data));
}

Mouse::~Mouse()
{
	d->reader->terminate();
	d->reader->wait();
}

qint8 Mouse::axisRate(Mouse::Axis axis)
{
	qint8 ret = d->oldData.axis[axis];
	d->oldData.axis[axis] = 0;
	d->hasMoved = false;
	return ret;
}

void Mouse::clearAxes()
{
	d->oldData.axis[0] = d->oldData.axis[1] = 0;
}

bool Mouse::hasMouseMoved() const
{
	return d->hasMoved;
}

void Mouse::updateInput(const Mouse::Data &input)
{
	quint8 newButtonsPressed = (~d->oldData.buttons & input.buttons);
	d->oldData = input;
	d->hasMoved = true;

	if(newButtonsPressed)
	{
		for(unsigned int i = 0;i < 5;++i)
		{
			if((newButtonsPressed & (1<<i)))
				emit buttonPressed(static_cast<Button>(i));
		}
	}
	if(input.scrollWheel > 0)
		emit buttonPressed(BUTTON_SCROLLDOWN);
	else if(input.scrollWheel < 0)
		emit buttonPressed(BUTTON_SCROLLUP);
}
