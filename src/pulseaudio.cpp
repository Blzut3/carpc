#include "lcddisplay.h"
#include "pulseaudio.h"

#include <pulse/context.h>
#include <pulse/introspect.h>
#include <pulse/thread-mainloop.h>
#include <pulse/volume.h>

#include <QDebug>

DClass<PulseAudio>
{
public:
	int channels;
	int volume;

	pa_threaded_mainloop *mainloop;
	pa_context *context;
};

DPointered(PulseAudio)

PulseAudio *PulseAudio::staticPulse;

class PACallbacks
{
public:
	static void contextNotify(pa_context *c, void *userdata)
	{
		PulseAudio * const pa = reinterpret_cast<PulseAudio*>(userdata);

		//qDebug() << "contextNotify" << pa_context_get_state(pa->d->context);

		static bool gotSink = false;
		if(!gotSink && pa_context_get_state(pa->d->context) == PA_CONTEXT_READY)
		{
			gotSink = true;
			pa_context_get_sink_info_by_index(pa->d->context, 0, PACallbacks::getSinkInfo, userdata);
		}
	}

	static void contextSuccess(pa_context *c, int success, void *userdata)
	{
		//qDebug() << "contextSuccess" << success;
	}

	static void getSinkInfo(pa_context *c, const pa_sink_info *i, int eol, void *userdata)
	{
		if(i == NULL) return;
		//qDebug() << "getSinkInfo" << i;

		PulseAudio * const pa = reinterpret_cast<PulseAudio*>(userdata);
		pa->d->channels = i->sample_spec.channels;

		pa->setVolume(0, true);
	}
};

PulseAudio::PulseAudio()
{
	if(!staticPulse)
		staticPulse = this;

	d->channels = 0;
	d->volume = 10;

	d->mainloop = pa_threaded_mainloop_new();
	pa_threaded_mainloop_start(d->mainloop);
	d->context = pa_context_new(pa_threaded_mainloop_get_api(d->mainloop), "carplayer Controller");
	pa_context_set_state_callback(d->context, PACallbacks::contextNotify, this);
	pa_context_connect(d->context, NULL, PA_CONTEXT_NOFLAGS, NULL);
}

PulseAudio::~PulseAudio()
{
	pa_context_disconnect(d->context);
	pa_threaded_mainloop_stop(d->mainloop);
	pa_threaded_mainloop_free(d->mainloop);
}

void PulseAudio::mouseButtonPressed(Mouse::Button button)
{
	if(button == Mouse::BUTTON_SCROLLUP)
		setVolume(5, true);
	else if(button == Mouse::BUTTON_SCROLLDOWN)
		setVolume(-5, true);
}

void PulseAudio::setVolume(int volume, bool relative)
{
	int oldvolume = d->volume;

	if(relative)
		d->volume += volume;
	else
		d->volume = volume;

	if(d->volume < 0)
		d->volume = 0;
	else if(d->volume > 100)
		d->volume = 100;

	if(d->volume != oldvolume)
		emit volumeChanged(d->volume);

	if(!d->channels)
		return;

	//qDebug() << "Setting volume to" << d->volume << d->channels;
	pa_cvolume paVolume;
	pa_cvolume_set(pa_cvolume_init(&paVolume), d->channels, pa_sw_volume_from_linear(static_cast<double>(d->volume)/200.0));
	pa_context_set_sink_volume_by_index(d->context, 0, &paVolume, PACallbacks::contextSuccess, this);
}

int PulseAudio::volume() const
{
	return d->volume;
}
