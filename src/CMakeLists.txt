find_package(Qt4 COMPONENTS QtCore QtDBus QtGui QtSql REQUIRED)
include(${QT_USE_FILE})

include_directories(${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_BINARY_DIR}/src ${DISPLAYDRIVER_INCLUDE_DIR} ${PostgreSQL_INCLUDE_DIR})

set(DB_SERVER "" CACHE STRING "Server address.")
set(DB_USERNAME "" CACHE STRING "Database username.")
set(DB_PASSWORD "" CACHE STRING "Database password.")
set(DB_DATABASE "" CACHE STRING "Database database.")
if(NOT DB_SERVER OR NOT DB_USERNAME OR NOT DB_PASSWORD OR NOT DB_DATABASE)
	message(SEND_ERROR "Database variables are not set.")
endif(NOT DB_SERVER OR NOT DB_USERNAME OR NOT DB_PASSWORD OR NOT DB_DATABASE)
add_definitions(-DDB_SERVER=${DB_SERVER} -DDB_USERNAME=${DB_USERNAME} -DDB_PASSWORD=${DB_PASSWORD} -DDB_DATABASE=${DB_DATABASE})

file(GLOB HEADER_FILES *.h interface/*.h dbus/*.h)

qt4_add_dbus_interfaces(INTERFACE_FILES
	dbus/udisks.xml
	dbus/udiskdevice.xml
)

qt4_add_resources(RESOURCE_FILES res/resources.qrc)

add_executable(carplayer
	cdripper.cpp
	database.cpp
	dbus.cpp
	fsmedia.cpp
	lcddisplay.cpp
	main.cpp
	media.cpp
	mediaplayer.cpp
	mouse.cpp
	pulseaudio.cpp
	uicontroller.cpp
	dbus/device.cpp
	interface/dialog.cpp
	interface/interface.cpp
	interface/mainmenumodel.cpp
	interface/mediamodel.cpp
	interface/menu.cpp
	interface/menumodel.cpp
	interface/playerpage.cpp
	${INTERFACE_FILES}
	${RESOURCE_FILES}
	${HEADER_FILES}
)

target_link_libraries(carplayer displaydriver vlc pulse cddb ${QT_LIBRARIES})

# Enable C++11
target_compile_features(carplayer PUBLIC cxx_range_for cxx_nullptr cxx_generalized_initializers cxx_rvalue_references)

set(OUTPUT_DIR ${CMAKE_BINARY_DIR} CACHE PATH "Directory where the executable will be created." )
set_target_properties(carplayer PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${OUTPUT_DIR})
