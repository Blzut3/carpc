#include "media.h"
#include "mediaplayer.h"

#include <QFile>
#include <QVariant>

DClass<Media>
{
public:
	unsigned int references;
	bool playing;

	QVariant artist;
	QVariant album;
	QVariant track;
	QVariant location;
};

DPointered(Media)

Media::Media(const QFile &file)
{
	d->playing = false;
	d->references = 0;
}

Media::Media()
{
	d->playing = false;
	d->references = 0;
}

Media::~Media()
{
	if(d->playing)
	{
		MediaPlayer::instance().stop();
		// If for some reason this doesn't stop the media object, please do so
		if(d->playing)
			stop();
	}
}

QVariant Media::getMeta(MetaInformation meta) const
{
	switch(meta)
	{
		default: break;
		case ARTIST: return d->artist;
		case ALBUM: return d->album;
		case TRACK: return d->track;
		case LOCATION: return d->location;
	}
	return QVariant();
}

bool Media::isPlaying() const
{
	return d->playing;
}

void Media::dereference()
{
	if(--d->references == 0)
		delete this;
}

void Media::doPlay(const QUrl &url, const QString &options, float start, float end)
{
	d->playing = true;

	MediaPlayer::instance().setMediaObject(this);
	MediaPlayer::instance().playMedia(url, options, start, end);
}

void Media::play()
{
}

void Media::reference()
{
	++d->references;
}

void Media::setMeta(MetaInformation meta, QVariant data)
{
	switch(meta)
	{
		default: break;
		case ARTIST:
			d->artist = data;
			break;
		case ALBUM:
			d->album = data;
			break;
		case TRACK:
			d->track = data;
			break;
		case LOCATION:
			d->location = data;
			break;
	}
	emit metaChanged(meta);
}

void Media::stop()
{
	d->playing = false;
}

void Media::finish()
{
	emit finished();
}

void Media::starve()
{
	emit starved();
}
