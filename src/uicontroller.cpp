#include "lcddisplay.h"
#include "mouse.h"
#include "uicontroller.h"
#include "interface/dialog.h"
#include "interface/interface.h"
#include "interface/mainmenumodel.h"
#include "interface/mediamodel.h"
#include "interface/menu.h"
#include "interface/menumodel.h"
#include "interface/playerpage.h"

#include <QDebug>
#include <QStringList>

QScopedPointer<UIController> UIController::staticInstance;

enum Animation
{
	ANIM_None,
	ANIM_SlideIn,
	ANIM_SlideOut
};

DClass<UIController>
{
	Private() : mouse("/dev/input/mice"), animating(ANIM_None) {}

	Interface interface;
	Menu *mainMenu;
	Menu *mediaMenu;
	PlayerPage *playerPage;
	Mouse mouse;

	Animation animating;
};

DPointeredNoCopy(UIController)

UIController::UIController()
{
	connect(&LCDDisplay::instance(), SIGNAL(vsync()), SLOT(doAnimations()));
	connect(&LCDDisplay::instance(), SIGNAL(buttonPressed(int)), SLOT(lcdButtonPressed(int)));
	connect(&d->mouse, SIGNAL(buttonPressed(Mouse::Button)), SLOT(mouseButtonPressed(Mouse::Button)));

	d->mainMenu = new Menu(tr("Main Menu"), this);
	connect(d->mainMenu, SIGNAL(itemSelected(const Menu::Item *)), SLOT(handleSelection()));
	connect(d->mainMenu, SIGNAL(backedOut()), SLOT(handlePageExit()));
	MainMenuModel *model = new MainMenuModel();
	d->mainMenu->setModel(model);

	d->interface.pushPage(d->mainMenu);

	MediaModel *mediaModel = new MediaModel();
	d->mediaMenu = new Menu(tr("Select Device"), this);
	d->mediaMenu->setModel(mediaModel);

	d->playerPage = new PlayerPage(this);
	//d->interface.pushPage(d->playerPage);
}

void UIController::doAnimations()
{
	if(d->animating == ANIM_None)
	{
		if(d->mouse.hasMouseMoved())
		{
			qint8 x = d->mouse.axisRate(Mouse::AXIS_X);
			qint8 y = d->mouse.axisRate(Mouse::AXIS_Y);
			d->interface.page()->mouseMoved(x, y);
			if(x < -16)
				handlePageExit();
			LCDDisplay::instance().wakeBacklight();
		}
		d->interface.page()->tick();
	}
	else
	{
		d->mouse.clearAxes();
	}

	d->interface.draw();

	handleAnimation();
}

void UIController::handleAnimation()
{
	static const double SLIDE_SPEED = 0.2;

	switch(d->animating)
	{
	default:
		d->animating = ANIM_None;
		break;
	case ANIM_SlideIn:
		d->interface.setSlide(qMax<double>(0.0, d->interface.slide()-SLIDE_SPEED));
		if(d->interface.slide() < 0.001)
			d->animating = ANIM_None;
		break;
	case ANIM_SlideOut:
		if(d->interface.slide() >= 0.990)
		{
			d->animating = ANIM_None;
			d->interface.popPage();
			d->interface.setSlide(0.0);
		}
		else
			d->interface.setSlide(qMin<double>(1.0, d->interface.slide()+SLIDE_SPEED));
		break;
	}
}

void UIController::handlePageExit()
{
	if(!d->interface.atRoot())
		d->animating = ANIM_SlideOut;
}

void UIController::handleSelection()
{
	if(d->mainMenu->selected() == 1) //Bluetooth)
		pushPage(new Dialog("Manage Bluetooth", "Function is disabled for demo", false, this));
	else if(d->mainMenu->selected() == 2)
		pushPage(d->mediaMenu);
	else
		pushPage(d->playerPage);
}

void UIController::lcdButtonPressed(int button)
{
	switch(button)
	{
		case LCDDisplay::BUTTON_Return:
			handlePageExit();
			break;
		case LCDDisplay::BUTTON_Home:
			break;

		// Pass the other three buttons on.
		default:
			d->interface.page()->handleButton(button);
			break;
	}
}

#include "pulseaudio.h"
void UIController::mouseButtonPressed(Mouse::Button button)
{
	switch(button)
	{
		default: break;
		case Mouse::BUTTON_SCROLLUP:
		case Mouse::BUTTON_SCROLLDOWN:
			PulseAudio::instance().mouseButtonPressed(button);
			break;
	}
}

void UIController::pushPage(InterfacePage *page)
{
	d->animating = ANIM_SlideIn;
	d->interface.setSlide(1.0);

	d->interface.pushPage(page);
}

void UIController::start()
{
	d->interface.draw();
}

void UIController::startController()
{
	staticInstance.reset(new UIController());

	staticInstance->start();
}
