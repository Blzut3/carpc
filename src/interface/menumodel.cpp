#include "interface/menumodel.h"

#include <QList>
#include <QStringList>

DClass<BasicMenuModel>
{
	QList<Menu::Item *> items;
};

DPointered(BasicMenuModel)

BasicMenuModel::~BasicMenuModel()
{
	clear();
}

void BasicMenuModel::clear()
{
	foreach(Menu::Item *item, d->items)
		delete item;
	d->items.clear();
}

unsigned int BasicMenuModel::count() const
{
	return d->items.size();
}

Menu::Item *BasicMenuModel::item(unsigned int row) const
{
	return d->items[row];
}

void BasicMenuModel::setItems(const QStringList &list)
{
	clear();
	foreach(const QString &str, list)
		d->items.append(new Menu::Item(str));
}
