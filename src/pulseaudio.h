#pragma once

#include "dptr.h"
#include "mouse.h"

#include <QObject>

class PulseAudio : public QObject
{
	Q_OBJECT

public:
	PulseAudio();
	~PulseAudio();

	static inline PulseAudio &instance() { return *staticPulse; }

	int volume() const;

public slots:
	void setVolume(int volume, bool relative);
	void mouseButtonPressed(Mouse::Button button);

signals:
	void volumeChanged(int value);

private:
	friend class PACallbacks;

	DPtr<PulseAudio> d;

	static PulseAudio *staticPulse;
};
