#pragma once

#include "dptr.h"
#include "media.h"
#include <QObject>

class MediaDevice;
class MediaMetaInfo;
class QFileInfo;

class FSScanner : public QObject
{
	Q_OBJECT
	DPtr<FSScanner> d;

public:
	FSScanner(MediaDevice *dev);
	~FSScanner();

	void scan();
public slots:
	void mediaRemoved();
};

class FSMedia : public Media
{
	Q_OBJECT
	DPtr<FSMedia> d;

public:
	FSMedia(MediaDevice *dev);
	~FSMedia();

public slots:
	void mediaRemoved();
	void play();

protected slots:
	void finish();
	void previousTrack();

private:
	friend class FSScanner;
	void addMedia(const MediaMetaInfo &);
	void checkCache();
};
