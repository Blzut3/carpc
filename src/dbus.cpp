#include "dbus.h"
#include "udisksinterface.h"
#include "dbus/device.h"

#include <QCoreApplication>
#include <QDBusConnection>
#include <QDebug>
#include <QSet>

using namespace org::freedesktop;

DClass<DBus>
{
public:
	UDisks *udisks;

	// Counts the number of CDs inserted since in theory we could hook up a
	// USB CD drive because why not?
	unsigned short cdStatus;

	QHash<QString, MediaDevice *> activeDevices;
	QSet<QString> opticalSet;
};

DPointered(DBus)

DBus *DBus::staticInstance;

DBus::DBus()
{
	if(!staticInstance)
		staticInstance = this;

	d->udisks = new UDisks("org.freedesktop.UDisks", "/org/freedesktop/UDisks", QDBusConnection::systemBus(), this);
	QDBusPendingReply<QList<QDBusObjectPath> > enumeration = d->udisks->EnumerateDevices();

	connect(d->udisks, SIGNAL(DeviceAdded(const QDBusObjectPath &)), SLOT(deviceAdded(const QDBusObjectPath &)));
	connect(d->udisks, SIGNAL(DeviceRemoved(const QDBusObjectPath &)), SLOT(deviceRemoved(const QDBusObjectPath &)));
	connect(d->udisks, SIGNAL(DeviceChanged(const QDBusObjectPath &)), SLOT(checkOpticalDrive(const QDBusObjectPath &)));
	qDebug() << "DBus ready!" << d->udisks->daemonVersion();

	d->cdStatus = 0;

	enumeration.waitForFinished();
	foreach(const QDBusObjectPath &path, enumeration.value())
	{
		bool valid = deviceAdded(path);

		// Count disc drives with CDs already in them.
		if(valid && d->opticalSet.contains(path.path()))
			++d->cdStatus;
	}
}

DBus::~DBus()
{
	// Release our media devices!
	for(auto dev : d->activeDevices)
		dev->remove();
}

QList<MediaDevice *> DBus::activeDevices() const
{
	QList<MediaDevice *> ret;
	for(MediaDevice *dev : d->activeDevices)
		ret.append(dev);
	return ret;
}

bool DBus::cdInDrive() const
{
	return d->cdStatus > 0;
}

void DBus::checkOpticalDrive(const QDBusObjectPath &device)
{
	// Optical drives don't disappear from the system when their media is
	// removed, instead they simply change state.  Contrast to say a Zip drive
	// which will create a new partition.
	if(!d->opticalSet.contains(device.path()))
		return;

	// Disc added or removed
	bool wasActive = d->activeDevices.contains(device.path());
	if(deviceAdded(device))
	{
		if(!wasActive)
		{
			++d->cdStatus;
			emit cdStatusChanged(d->cdStatus > 0);
		}
	}
	else
	{
		if(wasActive)
		{
			--d->cdStatus;
			emit cdStatusChanged(d->cdStatus > 0);

			d->activeDevices.remove(device.path());
		}
	}
}

// Returns true if the newly added device is usable for media.
bool DBus::deviceAdded(const QDBusObjectPath &device)
{
	bool retOptical = false;
	MediaDevice *dev = NULL;

	try
	{
		dev = new MediaDevice(device);
		retOptical = dev->isOptical();
	}
	catch(bool isOptical)
	{
		qDebug() << "Device" << device.path() << "rejected!";
		retOptical = isOptical;
	}

	if(dev)
	{
		d->activeDevices.insert(device.path(), dev);
		emit newDevice(dev);
	}

	if(retOptical)
		d->opticalSet.insert(device.path());

	return dev != NULL;
}

void DBus::deviceRemoved(const QDBusObjectPath &device)
{
	if(d->opticalSet.contains(device.path()))
		d->opticalSet.remove(device.path());

	if(d->activeDevices.contains(device.path()))
	{
		MediaDevice *dev = d->activeDevices.take(device.path());
		dev->remove();
	}
}
