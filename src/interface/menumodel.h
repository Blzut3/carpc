#pragma once

#include "interface/menu.h"

class QStringList;

class AbstractMenuItemModel : public QObject
{
	Q_OBJECT

public:
	using QObject::QObject;
	virtual ~AbstractMenuItemModel() {}

	virtual unsigned int count() const=0;
	virtual Menu::Item *item(unsigned int row) const=0;

signals:
	void modelUpdated();
};

class BasicMenuModel : public AbstractMenuItemModel
{
	Q_OBJECT

public:
	using AbstractMenuItemModel::AbstractMenuItemModel;
	~BasicMenuModel();

	void clear();
	unsigned int count() const;
	Menu::Item *item(unsigned int row) const;
	void setItems(const QStringList &list);

private:
	DPtr<BasicMenuModel> d;
};
