#include "interface/playerpage.h"
#include "lcddisplay.h"
#include "mediaplayer.h"

#include <QDebug>
#include <QString>

DClass<PlayerPage>
{
public:
	Media *mediaObject;

	bool metaUpdated;
	QString location;
	QString track;
};

DPointered(PlayerPage)

PlayerPage::PlayerPage(QObject *parent) : InterfacePage(parent)
{
	d->mediaObject = NULL;
	d->metaUpdated = false;

	MediaPlayer &mp = MediaPlayer::instance();
	connect(&mp, SIGNAL(mediaChanged(Media *)), SLOT(mediaChanged(Media *)));
}

PlayerPage::~PlayerPage()
{
	if(d->mediaObject)
		d->mediaObject->dereference();
}

void PlayerPage::draw(bool fullupdate)
{
	LCDDisplay &display = LCDDisplay::instance();

	if(fullupdate)
		d->metaUpdated = true;

	if(d->metaUpdated)
	{
		d->metaUpdated = false;

		qDebug() << d->location;
		display.fillRect(0, Interface::INFO_MARGINS+1, LCDDisplay::WIDTH, LCDDisplay::HEIGHT-Interface::INFO_MARGINS-1, 0);
		display.drawText(d->location, 0, 14);
		display.drawText(d->track, 0, 27, LCDDisplay::BIGFONT);
	}
}

void PlayerPage::handleButton(int button)
{
	if(button == LCDDisplay::BUTTON_Ok)
		MediaPlayer::instance().togglePause();
}

void PlayerPage::mediaChanged(Media *mediaObject)
{
	if(d->mediaObject)
		d->mediaObject->dereference();

	d->mediaObject = mediaObject;
	if(d->mediaObject)
	{
		mediaObject->reference();
		connect(mediaObject, SIGNAL(metaChanged(Media::MetaInformation)), SLOT(updateMeta(Media::MetaInformation)));

		d->metaUpdated = true;
		d->location = mediaObject->getMeta(Media::ARTIST).toString();
		if(d->location.isEmpty())
			d->location = mediaObject->getMeta(Media::LOCATION).toString();
		d->track = mediaObject->getMeta(Media::TRACK).toString();
	}
	else
	{
		d->metaUpdated = true;
		d->location = QString();
		d->track = QString();
	}
}

void PlayerPage::mouseMoved(int x, int y)
{
	if(qAbs(y) > 16)
	{
		if(y < 0)
			MediaPlayer::instance().nextTrack();
		else
			MediaPlayer::instance().prevTrack();
	}
}

QString PlayerPage::title() const
{
	return "Now Playing";
}

void PlayerPage::updateMeta(Media::MetaInformation meta)
{
	d->metaUpdated = true;
	switch(meta)
	{
		default: break;
		case Media::LOCATION:
		case Media::ALBUM:
			d->location = d->mediaObject->getMeta(Media::ALBUM).toString();
			if(d->location.isEmpty())
				d->location = d->mediaObject->getMeta(meta).toString();
			break;
		case Media::TRACK:
			d->track = d->mediaObject->getMeta(meta).toString();
			break;
	}
}
