#pragma once

#include "dptr.h"
#include "mouse.h"

#include <QObject>

class InterfacePage;

/**
 * Main logic controller for the interface.
 */
class UIController : public QObject
{
	Q_OBJECT

public:
	static UIController &instance() { return *staticInstance; }
	static void startController();

public slots:
	void doAnimations();

	void handlePageExit();
	void handleSelection();

protected:
	UIController();

	void handleAnimation();
	void start();
	void pushPage(InterfacePage *);

protected slots:
	void lcdButtonPressed(int button);
	void mouseButtonPressed(Mouse::Button);

private:
	static QScopedPointer<UIController> staticInstance;

	DPtr<UIController> d;
};
