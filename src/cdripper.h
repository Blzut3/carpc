#pragma once

#include "dptr.h"
#include "media.h"

#include <QObject>
#include <QProcess>

class MediaDevice;

class CDRipper : public QObject
{
	Q_OBJECT

public:
	CDRipper(MediaDevice *dev);
	~CDRipper();

public slots:
	void discRemoved();

signals:
	void ripFinished();

protected:
	void startRip();

protected slots:
	void paranoiaFinished(int, QProcess::ExitStatus);
	void paranoiaProgress();

private:
	friend class CDMedia;

	DPtr<CDRipper> d;
};

/**
 * Really shouldn't be used alone, but defined here so Qt can wrap.
 * 
 * Media object which handles transition between rips and possibly telling VLC
 * to do its own CD reading.
 */
class CDMedia : public Media
{
	Q_OBJECT

public:
	CDMedia(CDRipper *ripper);
	~CDMedia();

public slots:
	void play();

protected slots:
	void finish();
	void finishRip();

private:
	DPtr<CDMedia> d;
};
