#include "interface/mainmenumodel.h"

#include <QScopedPointer>

enum { ITEM_NOWPLAYING, ITEM_BLUETOOTH, ITEM_SELECT, NUM_FIXED_ITEMS };

DClass<MainMenuModel>
{
	QScopedPointer<Menu::Item> fixedItems[NUM_FIXED_ITEMS];
};

DPointeredNoCopy(MainMenuModel)

MainMenuModel::MainMenuModel(QObject *parent) : AbstractMenuItemModel(parent)
{
	d->fixedItems[ITEM_NOWPLAYING].reset(new Menu::Item("Now Playing"));
	d->fixedItems[ITEM_BLUETOOTH].reset(new Menu::Item("Manage Bluetooth"));
	d->fixedItems[ITEM_SELECT].reset(new Menu::Item("Select Track"));
}

MainMenuModel::~MainMenuModel()
{
}

unsigned int MainMenuModel::count() const
{
	return 3;
}

Menu::Item *MainMenuModel::item(unsigned int row) const
{
	return d->fixedItems[row].data();
}
