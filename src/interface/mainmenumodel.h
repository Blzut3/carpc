#pragma once

#include "interface/menumodel.h"

/**
 * Main menu model which handles devices that get added to/removed from the
 * system.
 */
class MainMenuModel : public AbstractMenuItemModel
{
	Q_OBJECT
	DPtr<MainMenuModel> d;

public:
	MainMenuModel(QObject *parent=nullptr);
	~MainMenuModel();

	unsigned int count() const;
	Menu::Item *item(unsigned int row) const;
};
