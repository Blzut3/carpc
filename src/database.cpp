#include "database.h"

#include <QDebug>
#include <QStringList>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>

DClass<Database>
{
public:
	QSqlDatabase db;
};

DPointered(Database)

Database::Database()
{
	d->db = QSqlDatabase::addDatabase("QPSQL");

	#define CONVAR2(str) #str
	#define CONVAR(str) CONVAR2(str)
	d->db.setHostName(CONVAR(DB_SERVER));
	d->db.setDatabaseName(CONVAR(DB_DATABASE));
	d->db.setUserName(CONVAR(DB_USERNAME));
	d->db.setPassword(CONVAR(DB_PASSWORD));
	#undef CONVAR
	#undef CONVAR2

	if(!d->db.open())
		qDebug() << "Failed to connect to database:" << d->db.lastError().text();
}

Database::~Database()
{
	d->db.close();
}

QStringList Database::artistNames() const
{
	QStringList ret;
	QSqlQuery artists("SELECT aid, name FROM artist WHERE aid != 1 OR (aid = 1 AND (SELECT count(*) FROM library.album WHERE artist = 1) > 1)");
	while(artists.next())
	{
		unsigned int id = artists.value(0).toUInt();
		QString name(artists.value(1).toString());
		ret << name;
	}
	return ret;
}
