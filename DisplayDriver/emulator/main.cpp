#include "emulator/main.h"
#include "picoLCD.h"

#include <QBoxLayout>
#include <QImage>
#include <QPainter>
#include <QPushButton>

class Emulator::EmulatorPrivate
{
public:
	uchar buffer[SCREEN_W*SCREEN_H];
};

Emulator::Emulator(QWidget *parent) : QWidget(parent)
{
	d = new EmulatorPrivate;
	memset(d->buffer, 1, SCREEN_W*SCREEN_H);
}

Emulator::~Emulator()
{
	delete d;
}

void Emulator::blit(const uchar *buf, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	for (unsigned int r = y; r < y + h; ++r) {
		memcpy(d->buffer + r*SCREEN_W + x, buf + r*SCREEN_W + x, w);
    }
	repaint();
}

void Emulator::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QImage image(d->buffer, SCREEN_W, SCREEN_H, SCREEN_W, QImage::Format_Indexed8);
	image.setNumColors(2);
	image.setColor(0, qRgb(127, 203, 229));
	image.setColor(1, qRgb(0, 102, 202));
	painter.drawImage(QRect(QPoint(0,0), size()), image);
}

////////////////////////////////////////////////////////////////////////////////

class EmulatorWindow::EmulatorWindowPrivate
{
public:
	Emulator *central;
	unsigned int buttonCode;
};

EmulatorWindow::EmulatorWindow() : d(new EmulatorWindowPrivate())
{
	d->buttonCode = 0;

	QLayout *mainLayout = new QHBoxLayout(),
		*leftLayout = new QVBoxLayout(),
		*rightLayout = new QVBoxLayout();

	QWidget *mainWidget = new QWidget();
	mainWidget->setLayout(mainLayout);
	QWidget *leftPanel = new QWidget();
	leftPanel->setLayout(leftLayout);
	QWidget *rightPanel = new QWidget();
	rightPanel->setLayout(rightLayout);

	d->central = new Emulator(this);
	d->central->setMaximumSize(512, 128);
	d->central->setMinimumSize(512, 128);

	mainLayout->addWidget(leftPanel);
	mainLayout->addWidget(d->central);
	mainLayout->addWidget(rightPanel);

	const struct
	{
		const char* text;
		QLayout *side;
		const char* slot;
	} buttons[5] = {
		{"Ret", leftLayout, SLOT(returnPressed())},
		{"Home", leftLayout, SLOT(homePressed())},
		{"Up", rightLayout, SLOT(upPressed())},
		{"OK", rightLayout, SLOT(okPressed())},
		{"Down", rightLayout, SLOT(downPressed())}
	};
	for(unsigned int i = 0;i < 5;++i)
	{
		QPushButton *button = new QPushButton(buttons[i].text);
		connect(button, SIGNAL(pressed()), buttons[i].slot);
		connect(button, SIGNAL(released()), SLOT(buttonReleased()));
		buttons[i].side->addWidget(button);
	};

	setCentralWidget(mainWidget);
}

EmulatorWindow::~EmulatorWindow()
{
	delete d;
}

void EmulatorWindow::blit(const uchar *buf, unsigned int x, unsigned int y, unsigned int w, unsigned int h)
{
	d->central->blit(buf, x, y, w, h);
}

void EmulatorWindow::buttonReleased()
{
	d->buttonCode = 0;
}

void EmulatorWindow::downPressed()
{
	d->buttonCode = 7;
}

int EmulatorWindow::gpiCode()
{
	//const int ret = d->buttonCode;
	//d->buttonCode = 0;
	return d->buttonCode;
}

void EmulatorWindow::homePressed()
{
	d->buttonCode = 2;
}

void EmulatorWindow::okPressed()
{
	d->buttonCode = 6;
}

void EmulatorWindow::returnPressed()
{
	d->buttonCode = 1;
}

void EmulatorWindow::upPressed()
{
	d->buttonCode = 5;
}
